#include "../FastTrackFinderLRTHypoTool.h"
#include "../FastTrackFinderLRTHypoAlg.h"
#include "../TrigIsoHPtTrackTriggerHypoAlg.h"
#include "../TrigIsoHPtTrackTriggerHypoTool.h"
#include "../TrigdEdxTrackTriggerHypoAlg.h"
#include "../TrigdEdxTrackTriggerHypoTool.h"

DECLARE_COMPONENT (TrigdEdxTrackTriggerHypoAlg )
DECLARE_COMPONENT (TrigdEdxTrackTriggerHypoTool )
DECLARE_COMPONENT (TrigIsoHPtTrackTriggerHypoAlg )
DECLARE_COMPONENT (TrigIsoHPtTrackTriggerHypoTool )
DECLARE_COMPONENT( FastTrackFinderLRTHypoTool )
DECLARE_COMPONENT( FastTrackFinderLRTHypoAlg )
